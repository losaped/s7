FROM golang:latest as builder

RUN mkdir /users

WORKDIR /users

COPY . .

WORKDIR /users/cmd/users
RUN CGO_ENABLED=0 GOOS=linux go build -mod vendor -a -installsuffix cgo .

FROM xordiv/docker-alpine-cron

RUN apk --no-cache add ca-certificates

RUN mkdir /app
WORKDIR /app
COPY --from=builder /users/cmd/users .

ENTRYPOINT ["./users"]

CMD ["./users"]