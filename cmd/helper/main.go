package main

import (
	"encoding/json"
	"fmt"
	"log"
	"time"

	"bitbucket.org/losaped/s7/gen/http/models"
	"github.com/go-openapi/strfmt"
)

func main() {
	CreateLoginRequest()
}

func CreateIndividualAccRequest() {
	req := new(models.NewIndividualRequest)
	email := "losaped@gmail.com"
	phone := "+79183691340"
	fName := "Roman"
	lName := "Poletaev"
	mName := "Victorovich"
	inn := "75319024209131902"
	pasAddress := "some address for test"
	// var pasDate *strfmt.Date
	// if err := pasDate.UnmarshalText([]byte("2009-07-30")); err != nil {
	// 	log.Fatal(err)
	// }

	pasDate := strfmt.Date(time.Now())
	pasNum := "0309211179"
	pasProd := "OUFMS"
	pass := "mypass"
	// var dateOfBirth *strfmt.Date
	// if err := dateOfBirth.UnmarshalText([]byte("1984-08-14")); err != nil {
	// 	log.Fatal(err)
	// }

	dateOfBirth := strfmt.Date(time.Date(1984, time.August, 14, 0, 0, 0, 0, time.UTC))
	req.Email = &email
	req.Phone = &phone
	req.FirstName = &fName
	req.LastName = &lName
	req.MiddleName = &mName
	req.Inn = inn
	req.PasportAddress = &pasAddress
	req.PasportDate = &pasDate
	req.PasportNum = &pasNum
	req.PasportProducer = &pasProd
	req.PasportSubdivisionCode = "202-335"
	req.Password = &pass
	req.DateOfBirth = &dateOfBirth

	obj, err := json.Marshal(req)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(string(obj))
}

func CreateLoginRequest() {
	req := new(models.LoginRequest)
	email := "losaped@gmail.com"
	pass := "mypass"
	req.Email = &email
	req.Password = &pass

	obj, err := json.Marshal(req)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(string(obj))
}
