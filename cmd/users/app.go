package main

import (
	"bitbucket.org/losaped/s7/pkg/auth/jwt"
	"bitbucket.org/losaped/s7/pkg/gorm"
	"bitbucket.org/losaped/s7/pkg/mail/gomail"
	"bitbucket.org/losaped/s7/pkg/redis"
	httpProvider "bitbucket.org/losaped/s7/providers/http"
	"bitbucket.org/losaped/s7/services/users"
	"bitbucket.org/losaped/s7/services/users/store"
	"github.com/facebookgo/inject"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"github.com/rs/zerolog"
)

// App binds various parts together
type App struct {
	AccountService *users.AccountService `inject:""`
	OTPRepository  *store.OTPRepository  `inject:""`
	Backend        *store.Backend        `inject:""`
	HTTPUsers      *httpProvider.Users   `inject:""`
}

func check(err error) {
	if err != nil {
		panic(err)
	}
}

// New App creation
func New(logger zerolog.Logger, config *Config) *App {
	var g inject.Graph
	app := &App{}
	check(g.Provide(
		&inject.Object{Value: app},
		&inject.Object{Value: logger.With().Str("component", "service").Logger(), Name: "service logger"},
		&inject.Object{Value: logger.With().Str("component", "db").Logger(), Name: "db logger"},
		&inject.Object{Value: logger.With().Str("component", "http").Logger(), Name: "http logger"},
		&inject.Object{Value: &gorm.Backend{Driver: config.Driver, DBURI: config.DBURI}},
		&inject.Object{Value: &redis.Backend{Address: config.RedisAddress, MaxIdle: config.RedisMaxIdle, IdleTimeoutSeconds: config.RedisIdleTimeoutSeconds}},
		&inject.Object{Value: gomail.New(config.Mail)},
		&inject.Object{Value: jwt.New(config.Secret)},
		&inject.Object{Value: config.HTTP},
	))
	check(g.Populate())
	logger.Debug().Msgf("%# v", app)
	return app
}

// Connect the app
func (app *App) Connect() error {
	if err := app.Backend.Gorm.Connect(); err != nil {
		return err
	}
	defer app.Backend.Gorm.Close()

	app.Backend.Redis.Connect()
	defer app.Backend.Redis.Close()

	app.HTTPUsers.Connect()
	return app.HTTPUsers.Serve()
}
