package main

import (
	"bitbucket.org/losaped/s7/pkg/mail"
	"bitbucket.org/losaped/s7/providers/http"
	"bitbucket.org/losaped/s7/services/users"
	_ "github.com/joho/godotenv/autoload" // preload .env
	"github.com/kelseyhightower/envconfig"

	"github.com/pkg/errors"
)

// Config for application
type Config struct {
	Debug                   bool                       `envconfig:"DEBUG" default:"true"`
	Driver                  string                     `envconfig:"DB_DRIVER"`
	DBURI                   string                     `envconfig:"DB_URI"`
	RedisAddress            string                     `envconfig:"REDIS_ADDR"`
	RedisMaxIdle            int                        `envconfig:"REDIS_MAX_IDLE"`
	RedisIdleTimeoutSeconds int                        `envconfig:"REDIS_IDLE_TIME"`
	Accounts                users.AccountServiceConfig `envconfig:"ACCOUNTS"`
	Mail                    *mail.Config               `envconfig:"MAIL"`
	Secret                  string                     `envconfig:"SECRET" default:"test secret"`
	HTTP                    *http.Config               `envconfig:"HTTP"`
}

// LoadConfig parses env and loads config
func LoadConfig(prefix string) (*Config, error) {
	res := new(Config)
	return res, errors.Wrap(envconfig.Process(prefix, res), "parse config")
}
