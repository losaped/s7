package main

import (
	"os"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

func main() {
	baseLogger := log.Output(zerolog.ConsoleWriter{Out: os.Stderr})
	logger := baseLogger.With().Str("service", "users").Logger()
	cfg, err := LoadConfig("")
	if err != nil {
		logger.Fatal().Err(err)
	}

	if cfg.Debug {
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	}

	logger.Debug().Msgf("SERVER PORT %+v", cfg.HTTP)

	app := New(logger, cfg)
	if err := app.Connect(); err != nil {
		logger.Fatal().Err(err)
	}
}
