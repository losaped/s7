// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the generate command

import (
	"net/http"

	middleware "github.com/go-openapi/runtime/middleware"
)

// CreateIndividualHandlerFunc turns a function with the right signature into a create individual handler
type CreateIndividualHandlerFunc func(CreateIndividualParams) middleware.Responder

// Handle executing the request and returning a response
func (fn CreateIndividualHandlerFunc) Handle(params CreateIndividualParams) middleware.Responder {
	return fn(params)
}

// CreateIndividualHandler interface for that can handle valid create individual params
type CreateIndividualHandler interface {
	Handle(CreateIndividualParams) middleware.Responder
}

// NewCreateIndividual creates a new http.Handler for the create individual operation
func NewCreateIndividual(ctx *middleware.Context, handler CreateIndividualHandler) *CreateIndividual {
	return &CreateIndividual{Context: ctx, Handler: handler}
}

/*CreateIndividual swagger:route POST /individual/create createIndividual

creates new individual

*/
type CreateIndividual struct {
	Context *middleware.Context
	Handler CreateIndividualHandler
}

func (o *CreateIndividual) ServeHTTP(rw http.ResponseWriter, r *http.Request) {
	route, rCtx, _ := o.Context.RouteInfo(r)
	if rCtx != nil {
		r = rCtx
	}
	var Params = NewCreateIndividualParams()

	if err := o.Context.BindValidRequest(r, route, &Params); err != nil { // bind params
		o.Context.Respond(rw, r, route.Produces, route, err)
		return
	}

	res := o.Handler.Handle(Params) // actually handle the request

	o.Context.Respond(rw, r, route.Produces, route, res)

}
