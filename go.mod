module bitbucket.org/losaped/s7

go 1.12

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/facebookgo/ensure v0.0.0-20160127193407-b4ab57deab51 // indirect
	github.com/facebookgo/inject v0.0.0-20180706035515-f23751cae28b
	github.com/facebookgo/stack v0.0.0-20160209184415-751773369052 // indirect
	github.com/facebookgo/structtag v0.0.0-20150214074306-217e25fb9691 // indirect
	github.com/facebookgo/subset v0.0.0-20150612182917-8dac2c3c4870 // indirect
	github.com/go-mixins/log v0.1.1 // indirect
	github.com/go-noodle/middleware v0.0.0-20171224161924-90e544ca5b3d // indirect
	github.com/go-noodle/noodle v0.0.0-20180314081423-c90fdeb956a9 // indirect
	github.com/go-noodle/store v0.0.0-20171224161946-46cde2e548fd // indirect
	github.com/go-noodle/wok v0.0.0-20190511214527-da639df549b8 // indirect
	github.com/go-openapi/errors v0.19.0
	github.com/go-openapi/loads v0.18.0
	github.com/go-openapi/runtime v0.19.0
	github.com/go-openapi/spec v0.18.0
	github.com/go-openapi/strfmt v0.19.0
	github.com/go-openapi/swag v0.19.0
	github.com/go-openapi/validate v0.19.0
	github.com/go-swagger/go-swagger v0.19.0 // indirect
	github.com/gomodule/redigo v1.7.0
	github.com/jessevdk/go-flags v1.4.0
	github.com/jinzhu/gorm v1.9.2
	github.com/joho/godotenv v1.3.0
	github.com/julienschmidt/httprouter v1.2.0 // indirect
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/pkg/errors v0.8.1
	github.com/rs/xid v1.2.1
	github.com/rs/zerolog v1.14.3
	golang.org/x/crypto v0.0.0-20190320223903-b7391e95e576
	golang.org/x/net v0.0.0-20190320064053-1272bf9dcd53
	gopkg.in/alexcesaro/quotedprintable.v3 v3.0.0-20150716171945-2caba252f4dc // indirect
	gopkg.in/gomail.v2 v2.0.0-20160411212932-81ebce5c23df
	gopkg.in/gormigrate.v1 v1.5.0
)
