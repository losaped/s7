package auth

import "errors"

var (
	ErrUnauthorized = errors.New("unauthorized")
	ErrTokenExpired = errors.New("token is expired")
)
