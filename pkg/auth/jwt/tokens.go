package jwt

import (
	"sync"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/pkg/errors"
	"github.com/rs/xid"

	"bitbucket.org/losaped/s7/pkg/auth"
)

// Tokens implements basic services functions and middlewares
type Tokens struct {
	SigningMethod jwt.SigningMethod
	UUID          func() string
	TimeToLive    time.Duration
	Secret        string
	Now           func() time.Time

	mu sync.RWMutex
}

var _ auth.Tokens = (*Tokens)(nil)

type Claims struct {
	jwt.StandardClaims
	*auth.SystemClaims
}

// New creates new concrete services.Tokens instance
func New(secret string) *Tokens {
	return &Tokens{
		SigningMethod: jwt.SigningMethodHS256,
		UUID:          func() string { return xid.New().String() },
		Secret:        secret,
		TimeToLive:    365 * 24 * time.Hour,
	}
}

// Verify token
func (p *Tokens) Verify(token string) (*auth.SystemClaims, error) {
	var res Claims
	parser := &jwt.Parser{SkipClaimsValidation: true}
	_, err := parser.ParseWithClaims(token, &res, func(token *jwt.Token) (interface{}, error) {
		return []byte(p.Secret), nil
	})
	if err != nil {
		return nil, errors.Wrap(auth.ErrUnauthorized, "parse claims")
	}
	switch {
	case !res.VerifyExpiresAt(jwt.TimeFunc().Unix(), true):
		err = auth.ErrTokenExpired
	default:
		if err = res.Valid(); err != nil {
			err = auth.ErrUnauthorized
		}
	}
	if err != nil {
		return nil, errors.Wrap(err, "get claims")
	}
	return res.SystemClaims, nil
}

// SignToken creates string from user
func (p *Tokens) SignToken(u *auth.SystemClaims) (string, error) {
	now := jwt.TimeFunc()
	claims := &Claims{
		StandardClaims: jwt.StandardClaims{
			Id:        p.UUID(),
			ExpiresAt: now.Add(p.TimeToLive).Unix(),
		},
		SystemClaims: u,
	}
	token := jwt.NewWithClaims(p.SigningMethod, claims)
	res, err := token.SignedString([]byte(p.Secret))
	return res, errors.Wrap(err, "sign token")
}
