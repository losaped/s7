package auth

import (
	"context"
)

// Tokens provides auth tokens
type Tokens interface {
	Verify(token string) (*SystemClaims, error)
	SignToken(src *SystemClaims) (string, error)
}

type SystemClaims struct {
	UserID    uint64 `json:"uid"`
	Confirmed bool   `json:"confirmed"`
	Type      int    `json:"type"`
}

type claimsKey struct{}

// WithClaims injects claims into context
func WithClaims(ctx context.Context, claims *SystemClaims) context.Context {
	return context.WithValue(ctx, claimsKey{}, claims)
}

// Get extracts claims from context
func Get(ctx context.Context) *SystemClaims {
	res, _ := ctx.Value(claimsKey{}).(*SystemClaims)
	return res
}
