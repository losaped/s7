package gorm

import (
	"fmt"
	"strings"

	"github.com/jinzhu/gorm"
	"github.com/pkg/errors"
	"github.com/rs/zerolog"
)

// Backend implements generic database backend
type Backend struct {
	DB     *gorm.DB
	Driver string
	DBURI  string
	Debug  bool
	Log    zerolog.Logger `inject:"db logger"`
}

type printer struct {
	zerolog.Logger
}

type Printer interface {
	Print(v ...interface{})
}

// Logger wraps ContextLogger for use with GORM
func Logger(logger zerolog.Logger) Printer {
	return &printer{Logger: logger}
}

func (gl printer) Print(vals ...interface{}) {
	var res []string
	for _, v := range vals {
		res = append(res, fmt.Sprint(v))
	}
	gl.Logger.Debug().Msg(strings.Join(res, " "))
}

func (b *Backend) driver() string {
	if b.Driver != "" {
		return b.Driver
	}
	return "sqlite3"
}

func (b *Backend) dbURI() string {
	driver := b.driver()
	if b.DBURI == "" {
		return "test"
	}
	if driver == "postgres" {
		return b.DBURI + " binary_parameters=yes"
	} else if driver == "mysql" {
		return b.DBURI + "?charset=utf8&parseTime=True&loc=Local"
	}
	return b.DBURI
}

// Connect sets up the backend
func (b *Backend) Connect() error {
	db, err := gorm.Open(b.driver(), b.dbURI())
	if err != nil {
		return errors.Wrap(err, "create database connection")
	}

	if db == nil {
		return nil
	}

	db.SetLogger(Logger(b.Log))
	if b.Debug {
		db.LogMode(true)
	}

	b.DB = db
	if err := b.Migrate(); err != nil {
		return errors.Wrap(err, "apply migrations")
	}

	return nil
}

// Close DB connection
func (b *Backend) Close() error {
	b.Log.Info().Msg("CLOSE POSTGRES CONNECTION")
	return errors.Wrap(b.DB.Close(), "closing database connection")
}
