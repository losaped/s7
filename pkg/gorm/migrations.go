package gorm

import (
	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"
)

// Migrate run migrations
func (b *Backend) Migrate() (err error) {

	m := gormigrate.New(b.DB, gormigrate.DefaultOptions, []*gormigrate.Migration{
		{
			ID: "0",
			Migrate: func(tx *gorm.DB) error {
				type Account struct {
					ID             uint64 `gorm:"primary_key"`
					Email          string `gorm:"type:varchar(100);unique_index;not_null"`
					Phone          string `gorm:"type:varchar(20);unique_index;not_null"`
					Password       string `gorm:"not_null"`
					Type           int
					EmailConfirmed bool `gorm:"not_null"`
					PhoneConfirmed bool `gorm:"not_null"`
					Deleted        bool `gorm:"not_null"`
				}

				return tx.AutoMigrate(&Account{}).Error
			},
			Rollback: func(tx *gorm.DB) error {
				return tx.DropTable("accounts").Error
			},
		},
	})

	b.DB.LogMode(true)
	defer b.DB.LogMode(false)
	return m.Migrate()
}
