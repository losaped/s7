package gomail

import (
	"bitbucket.org/losaped/s7/pkg/mail"
	"github.com/pkg/errors"
	gomail "gopkg.in/gomail.v2"
)

var _ mail.Sender = (*Sender)(nil)

// Sender implements mail.Sender using gomail package
type Sender struct {
	dialer *gomail.Dialer
	*mail.Config
}

// New creates new instance of Sender
func New(cfg *mail.Config) *Sender {
	if cfg.ContentType == "" {
		cfg.ContentType = "text/html"
	}
	if cfg.Port == 0 {
		cfg.Port = 587
	}
	res := &Sender{
		Config: cfg,
		dialer: gomail.NewDialer(cfg.Host, cfg.Port, cfg.Username, cfg.Password),
	}
	return res
}

// Send email to recipient
func (s *Sender) Send(to, subject, msg string) error {
	if s.From == "" {
		return errors.New(`empty "From"`)
	}
	m := gomail.NewMessage()
	m.SetHeader("From", s.From)
	m.SetHeader("To", to)
	m.SetHeader("Subject", subject)
	m.SetBody(s.ContentType, msg)
	return errors.Wrap(s.dialer.DialAndSend(m), "send msg")
}
