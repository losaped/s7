package mail

type Sender interface {
	Send(to, subject, message string) error
}

type Config struct {
	Host        string `envconfig:"HOST"`
	Port        int    `envconfig:"PORT"`
	Username    string `envconfig:"USERNAME" required:"true"`
	Password    string `envconfig:"PASSWORD" required:"true"`
	From        string `envconfig:"FROM" required:"true"`
	ContentType string
}