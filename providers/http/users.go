package http

import (
	"context"
	"log"
	"net/http"
	"strconv"

	"bitbucket.org/losaped/s7/gen/http/models"
	"bitbucket.org/losaped/s7/gen/http/restapi"
	"bitbucket.org/losaped/s7/gen/http/restapi/operations"
	"bitbucket.org/losaped/s7/services/users"
	"github.com/go-openapi/loads"
	"github.com/go-openapi/runtime/middleware"
	"github.com/go-openapi/swag"
	"github.com/pkg/errors"
	"github.com/rs/zerolog"
)

type Config struct {
	Port int `envconfig:"port"`
}

type Users struct {
	Log             zerolog.Logger `inject:"http logger"`
	server          *restapi.Server
	Config          *Config               `inject:""`
	AccountsService *users.AccountService `inject:""`
}

func (prov *Users) Connect() {
	swaggerSpec, err := loads.Analyzed(restapi.SwaggerJSON, "")
	if err != nil {
		log.Fatalln(err)
	}

	// create new service API
	api := operations.NewAuthServiceAPI(swaggerSpec)
	api.Logger = prov.Log.Info().Msgf
	server := restapi.NewServer(api)

	server.Port = prov.Config.Port

	prov.server = server

	api.CreateEntityHandler = operations.CreateEntityHandlerFunc(prov.CreateEntity)
	api.CreateIndividualHandler = operations.CreateIndividualHandlerFunc(prov.CreateIndividual)
	api.ConfirmAccountHandler = operations.ConfirmAccountHandlerFunc(prov.ConfirmAccount)
	api.LoginHandler = operations.LoginHandlerFunc(prov.Login)
}

func (prov *Users) Serve() error {
	return prov.server.Serve()
}
func (prov *Users) Close() {
	prov.server.Shutdown()
}

func (prov *Users) CreateEntity(params operations.CreateEntityParams) middleware.Responder {
	acc := users.Account{
		Email:    swag.StringValue(params.Form.ContactPersonEmail),
		Phone:    swag.StringValue(params.Form.ContactPersonPhone),
		Password: swag.StringValue(params.Form.Password),
	}

	newAcc, err := prov.AccountsService.Create(context.Background(), acc)
	if err == nil {
		payload := &models.Account{
			AccountType: "entity",
			ID:          strconv.Itoa(int(newAcc.ID)),
			Email:       newAcc.Email,
			Phone:       newAcc.Phone,
		}
		return operations.NewCreateEntityCreated().WithPayload(payload)
	}

	if errors.Cause(err) == users.ErrAlreadyExists {
		return operations.NewCreateEntityDefault(http.StatusBadRequest).WithPayload(&models.Error{
			Message: err.Error(),
			Code:    http.StatusBadRequest,
		})
	}

	return operations.NewCreateEntityDefault(http.StatusInternalServerError)

}

func (prov *Users) CreateIndividual(params operations.CreateIndividualParams) middleware.Responder {
	acc := users.Account{
		Email:    swag.StringValue(params.Form.Email),
		Phone:    swag.StringValue(params.Form.Phone),
		Password: swag.StringValue(params.Form.Password),
	}

	prov.Log.Debug().Msgf("NEW ACCOUNT %+v", acc)
	newAcc, err := prov.AccountsService.Create(context.Background(), acc)
	if err == nil {
		prov.Log.Debug().Msg("account created without errors")
		payload := &models.Account{
			AccountType: "individual",
			ID:          strconv.Itoa(int(newAcc.ID)),
			Email:       newAcc.Email,
			Phone:       newAcc.Phone,
		}
		return operations.NewCreateEntityCreated().WithPayload(payload)
	}

	if errors.Cause(err) == users.ErrAlreadyExists {
		return operations.NewCreateEntityDefault(http.StatusBadRequest).WithPayload(&models.Error{
			Message: err.Error(),
			Code:    http.StatusBadRequest,
		})
	}

	prov.Log.Error().Err(err)
	return operations.NewCreateEntityDefault(http.StatusInternalServerError)
}

func (prov *Users) ConfirmAccount(params operations.ConfirmAccountParams) middleware.Responder {
	if params.Source != string(users.TokenTypeConfirmEmal) && params.Source != string(users.TokenTypeConfirmPhone) {
		return operations.NewConfirmAccountDefault(http.StatusBadRequest).WithPayload(&models.Error{
			Code:    http.StatusBadRequest,
			Message: "source should be one of 'phone' and 'email'",
		})
	}

	otp := users.OTP{
		AccountID: uint64(params.ID),
		Type:      users.TokenType(params.Source),
		Password:  params.Otp,
	}

	ctx := context.Background()
	tokens, err := prov.AccountsService.Confirm(ctx, otp)
	if err != nil {
		if errors.Cause(err) == users.ErrNotFound {
			return operations.NewConfirmAccountUnauthorized()
		}

		return operations.NewConfirmAccountDefault(http.StatusInternalServerError)
	}

	return operations.NewConfirmAccountOK().WithPayload(&models.AuthResponse{
		AccessToken:  tokens.AccessToken,
		RefreshToken: tokens.RefreshToken,
	})
}

func (prov *Users) Login(params operations.LoginParams) middleware.Responder {
	ctx := context.Background()
	tokens, err := prov.AccountsService.Login(
		ctx,
		swag.StringValue(params.Form.Email),
		swag.StringValue(params.Form.Password))

	if err != nil {
		if errors.Cause(err) == users.ErrNotFound {
			return operations.NewLoginUnauthorized()
		}

		return operations.NewLoginDefault(http.StatusInternalServerError)
	}

	return operations.NewLoginOK().WithPayload(&models.AuthResponse{
		AccessToken:  tokens.AccessToken,
		RefreshToken: tokens.RefreshToken,
	})
}
