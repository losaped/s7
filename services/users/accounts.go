package users

import (
	"context"
	"fmt"
	"math/rand"
	"time"

	"golang.org/x/crypto/bcrypt"

	"bitbucket.org/losaped/s7/pkg/auth"
	"bitbucket.org/losaped/s7/pkg/mail"
	"github.com/pkg/errors"
)

type AccountType uint8

const (
	AccountTypeLegalEntity = 1
	AccountTypeIndividual  = 2
)

type LoginResponse struct {
	AccessToken  string
	RefreshToken string
}

type Account struct {
	ID             uint64
	Email          string `gorm:"type:varchar(100);unique_index;not_null"`
	Phone          string `gorm:"type:varchar(20);unique_index;not_null"`
	Password       string `gorm:"not_null"`
	Type           int
	EmailConfirmed bool `gorm:"not_null"`
	PhoneConfirmed bool `gorm:"not_null"`
	Deleted        bool `gorm:"not_null"`
}

const (
	otpEmailTemplate = "confirm code %s"
)

// AccountServiceConfig config
type AccountServiceConfig struct {
	OTPTTLSeconds int `envconfig:"OTP_TTL"`
	OTPCodeLength int `envconfig:"OTP_LENGTH"`
}

type Sender interface {
	Send(address, message string) error
}

// AccountService service struct
type AccountService struct {
	*AccountServiceConfig
	Backend     Backend     `inject:""`
	EmailSender mail.Sender `inject:""`
	Auth        auth.Tokens `inject:""`
	// Generator   OTPGenerator `inject:""`
	GenerateFunc func(int) string
}

// Create add new account record and send confirm email
func (s *AccountService) Create(ctx context.Context, acc Account) (*Account, error) {
	hash, _ := bcrypt.GenerateFromPassword([]byte(acc.Password), bcrypt.DefaultCost)
	acc.Password = string(hash)
	newacc, err := s.Backend.Accounts().Create(acc)
	if err != nil {
		return nil, errors.Wrap(err, "create account")
	}

	otp := OTP{
		AccountID: acc.ID,
		Type:      TokenTypeConfirmEmal,
		Password:  s.Generate(s.OTPCodeLength),
	}

	if err := s.Backend.OTPs().New(otp, s.OTPTTLSeconds); err != nil {
		return nil, errors.Wrap(err, "create otp")
	}

	return newacc, errors.Wrap(s.EmailSender.Send(acc.Email, "Email confirmation", fmt.Sprintf(otpEmailTemplate, otp.Password)), "sending confirm email")
}

// Confirm set phone or email as confirmed
func (s *AccountService) Confirm(ctx context.Context, otp OTP) (*LoginResponse, error) {
	acc, err := s.Backend.Accounts().GetByID(otp.AccountID)
	if err != nil {
		return nil, err
	}

	if _, err := s.Backend.OTPs().Get(otp); err != nil {
		return nil, errors.Wrap(err, "otp not found")
	}

	var cf func(uint64) error
	switch otp.Type {
	case TokenTypeConfirmEmal:
		cf = s.Backend.Accounts().ConfirmEmail
	case TokenTypeConfirmPhone:
		cf = s.Backend.Accounts().ConfirmPhone
	}

	if err := cf(otp.AccountID); err != nil {
		return nil, errors.Wrap(err, "confirm source")
	}

	if err := s.Backend.OTPs().Delete(otp); err != nil {
		return nil, errors.Wrap(err, "delete confirmed otp")
	}

	claims := &auth.SystemClaims{
		UserID:    otp.AccountID,
		Confirmed: true,
		Type:      acc.Type,
	}

	return s.login(claims)
}

// Login check email and pass and returns token
func (s *AccountService) Login(ctx context.Context, email, password string) (*LoginResponse, error) {
	acc, err := s.Backend.Accounts().GetByEmail(email)
	if err != nil {
		return nil, err
	}

	if err := bcrypt.CompareHashAndPassword([]byte(acc.Password), []byte(password)); err != nil {
		return nil, err
	}

	claims := &auth.SystemClaims{
		UserID:    acc.ID,
		Confirmed: acc.EmailConfirmed || acc.PhoneConfirmed,
		Type:      acc.Type,
	}

	return s.login(claims)
}

func (s *AccountService) Logout(ctx context.Context, token string) error {
	return ErrNotImplemented
}

// Delete revoke tokens for account and mark account as deleted
func (s *AccountService) Delete(ctx context.Context, id uint64) error {
	// if err := s.Backend.Tokens().DeleteByAccount(id); err != nil {
	// 	return errors.Wrap(err, "revoke tokens for account")
	// }

	return errors.Wrap(s.Backend.Accounts().Delete(id), "delete account")
}

func (s *AccountService) login(claims *auth.SystemClaims) (*LoginResponse, error) {
	token, err := s.Backend.Tokens().New(claims)
	if err != nil {
		return nil, err
	}

	jwt, err := s.Auth.SignToken(claims)
	if err != nil {
		return nil, err
	}

	return &LoginResponse{
		AccessToken:  jwt,
		RefreshToken: token,
	}, nil
}

type SimpleOTPGenerator struct {
}

func (s *AccountService) generate(length int) string {
	rand.Seed(time.Now().UnixNano())
	const letters = "0123456789"

	bytes := make([]byte, length)
	for i := range bytes {
		bytes[i] = byte(letters[rand.Intn(len(letters))])
	}

	return string(bytes)
}

func (s *AccountService) Generate(length int) string {
	if s.GenerateFunc == nil {
		return s.generate(length)
	}

	return s.GenerateFunc(length)
}
