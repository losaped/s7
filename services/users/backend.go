package users

type Backend interface {
	Accounts() AccountsRepository
	OTPs() OTPRepository
	Tokens() TokensRepository
}

// AccountsRepository account storage
type AccountsRepository interface {
	Create(Account) (*Account, error)
	ConfirmPhone(id uint64) error
	ConfirmEmail(id uint64) error
	GetByID(id uint64) (*Account, error)
	GetByEmail(email string) (*Account, error)
	Delete(id uint64) error
}
