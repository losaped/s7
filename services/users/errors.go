package users

import (
	"github.com/pkg/errors"
)

var ErrAlreadyExists = errors.New("already exists")
var ErrNotFound = errors.New("not found")
var ErrNotImplemented = errors.New("not implemented")
