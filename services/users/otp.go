package users

type TokenType string

const (
	TokenTypeConfirmEmal  TokenType = "email"
	TokenTypeConfirmPhone TokenType = "phone"
)

type OTPGenerator interface {
	Generate(len int) string
}

// OTP is a one-time password
type OTP struct {
	AccountID uint64
	Password  string
	Type      TokenType
}

func (o *OTP) String() string {
	return o.Password
}

// OTPRepository holds OTP tokens
type OTPRepository interface {
	New(otp OTP, TTLSecond int) error
	Get(otp OTP) (*OTP, error)
	Delete(otp OTP) error
}
