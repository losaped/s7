package store

import (
	"bitbucket.org/losaped/s7/services/users"
	"github.com/pkg/errors"
)

// AccountsRepository postgres implementation
type AccountsRepository Backend

func (r *Backend) Accounts() users.AccountsRepository {
	return (*AccountsRepository)(r)
}

// WithTx returns new instance of repository where tx will be used as db
// it allows call some functions in one transaction
// func (r *AccountsRepository) WithTx(tx *gorm.DB) *AccountsRepository {
// 	newInstance := *r
// 	newInstance.db = tx
// 	return &newInstance
// }

// Create creates new account
func (r *AccountsRepository) Create(acc users.Account) (*users.Account, error) {
	r.Gorm.DB.LogMode(true)
	defer r.Gorm.DB.LogMode(false)

	if err := r.Gorm.DB.Create(&acc).Error; err != nil {
		r.Log.Error().Err(err)
		if UniqueViolation(err) {
			return nil, errors.Wrap(users.ErrAlreadyExists, "account already exists")
		}
		return nil, accessDB(err)
	}

	return &acc, nil
}

// ConfirmPhone allows confirm phone
func (r *AccountsRepository) ConfirmPhone(id uint64) error {
	if err := r.Gorm.DB.Model(&users.Account{}).Where("id=?", id).Update("phone_confirmed", true).Error; err != nil {
		if NotFound(err) {
			return errors.Wrap(err, "account not found")
		}

		return accessDB(err)
	}

	return nil
}

// ConfirmEmail allows confirm email
func (r *AccountsRepository) ConfirmEmail(id uint64) error {
	if err := r.Gorm.DB.Model(&users.Account{}).Where("id=?", id).Update("email_confirmed", true).Error; err != nil {
		if NotFound(err) {
			return errors.Wrap(err, "account not found")
		}

		return accessDB(err)
	}

	return nil
}

// GetByID returns account with id
func (r *AccountsRepository) GetByID(id uint64) (*users.Account, error) {
	acc := &users.Account{}
	if err := r.Gorm.DB.First(&acc, 10).Error; err != nil {
		if NotFound(err) {
			return nil, errors.Wrap(err, "account not found")
		}

		return nil, accessDB(err)
	}

	return acc, nil
}

// GetByEmail returns account with email
func (r *AccountsRepository) GetByEmail(email string) (*users.Account, error) {
	acc := &users.Account{}
	if err := r.Gorm.DB.Where("email = ? and deleted = ?", email, false).First(acc).Error; err != nil {
		if NotFound(err) {
			return nil, errors.Wrap(err, "account not found")
		}

		return nil, accessDB(err)
	}

	return acc, nil
}

// Delete mark account as deleted
func (r *AccountsRepository) Delete(id uint64) error {
	return r.Gorm.DB.Model(&users.Account{}).Where("id=?", id).Update("deleted", true).Error
}
