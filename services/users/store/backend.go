package store

import (
	"bitbucket.org/losaped/s7/pkg/gorm"
	"bitbucket.org/losaped/s7/pkg/redis"
	"bitbucket.org/losaped/s7/services/users"
	"github.com/rs/zerolog"
)

type Backend struct {
	Log   zerolog.Logger `inject:"db logger"`
	Gorm  *gorm.Backend  `inject:""`
	Redis *redis.Backend `inject:""`
}

var _ users.Backend = (*Backend)(nil)
