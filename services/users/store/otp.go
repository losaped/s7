package store

import (
	"fmt"

	"bitbucket.org/losaped/s7/services/users"
	"github.com/gomodule/redigo/redis"
)

type OTPRepository Backend

// OTPs implement Backend OTPs
func (repo *Backend) OTPs() users.OTPRepository {
	return (*OTPRepository)(repo)
}

func (repo *OTPRepository) New(otp users.OTP, TTLSecond int) error {
	conn := repo.Redis.Pool.Get()
	defer conn.Close()

	_, err := conn.Do("set", fmt.Sprintf("%d:%s", otp.AccountID, otp.Type), otp.Password, "ex", TTLSecond)
	return err
}

func (repo *OTPRepository) Get(otp users.OTP) (*users.OTP, error) {
	conn := repo.Redis.Pool.Get()
	defer conn.Close()

	pass, err := redis.String(conn.Do("get", fmt.Sprintf("%d:%s", otp.AccountID, otp.Type)))
	if err != nil {
		if err == redis.ErrNil {
			return nil, users.ErrNotFound
		}
		return nil, err
	}

	otp.Password = pass
	return &otp, nil
}

func (repo *OTPRepository) Delete(otp users.OTP) error {
	conn := repo.Redis.Pool.Get()
	defer conn.Close()
	_, err := conn.Do("del", fmt.Sprintf("%d:%s", otp.AccountID, otp.Type))
	return err
}
