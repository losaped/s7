package store

import (
	"encoding/json"
	"fmt"

	"bitbucket.org/losaped/s7/pkg/auth"
	"bitbucket.org/losaped/s7/services/users"
	"github.com/gomodule/redigo/redis"
	"github.com/pkg/errors"
	"github.com/rs/xid"
)

const tokenKeyTemplate = "token:%s"

type TokensRepository Backend

func (repo *Backend) Tokens() users.TokensRepository {
	return (*TokensRepository)(repo)
}

func (t *TokensRepository) uuid() string {
	// if t.UUID != nil {
	// 	return t.UUID()
	// }
	return xid.New().String()
}

var _ users.TokensRepository = (*TokensRepository)(nil)

func (t *TokensRepository) New(val *auth.SystemClaims) (string, error) {
	c := t.Redis.Pool.Get()
	defer c.Close()

	bts, err := json.Marshal(val)
	if err != nil {
		return "", errors.Wrap(err, "marshal claims")
	}

	token := t.uuid()
	_, err = c.Do("set", fmt.Sprintf(tokenKeyTemplate, token), bts)

	return token, errors.Wrap(err, "save token")
}

func (t *TokensRepository) Get(token string) (*auth.SystemClaims, error) {
	c := t.Redis.Pool.Get()
	defer c.Close()

	res, err := redis.String(c.Do("get", fmt.Sprintf(tokenKeyTemplate, token)))
	if err != nil {
		if err == redis.ErrNil {
			err = users.ErrNotFound
		}

		return nil, errors.Wrap(err, "get token")
	}

	claims := &auth.SystemClaims{}
	return claims, errors.Wrap(json.Unmarshal([]byte(res), claims), "unmarshal token")
}

func (t *TokensRepository) Delete(token string) error {
	c := t.Redis.Pool.Get()
	defer c.Close()

	_, err := c.Do("del", fmt.Sprintf(tokenKeyTemplate, token))
	return errors.Wrap(err, "delete token")
}
