package users

import (
	"bitbucket.org/losaped/s7/pkg/auth"
)

type TokensRepository interface {
	New(claims *auth.SystemClaims) (string, error)
	Get(token string) (*auth.SystemClaims, error)
	Delete(token string) error
}
